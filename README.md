# Modèle CI/CD

Ce dépot contient le squelette à utiliser pour creer de nouveaux projets.

Afin de l'utiliser sur un nouveau projet, il faudra tout d'abord modifier la valeur de la variable IMAGE dans le fichier .gitlab-ci.yaml .

Il sera egalement necessaire de modifier le contenu des manifests.Le workflow est défini dans la documentation

## Variables

Un certain nombre de variables sont à définir dans le projet gitlab.

 - PROJECT_ID
	 - Cette variable doit contenir l'id du projet Google Cloud 
- SA
	- Cette variable doit contenir la clé json du compte de service Google Cloud qui sera utilisé pour ce projet
